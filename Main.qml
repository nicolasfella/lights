import QtQuick
import org.kde.lights

Window {
    id: root
    visible: true

    enum Color {
        Red,
        Green,
        Blue,
        White
    }

    property Main.Color color: Main.Color.Red

    Rectangle {
        color: Qt.rgba(1, 1 - handler.point.pressure, 1 - handler.point.pressure, 1)
        // parent: root
        anchors.fill: parent
    }

    TabletEvents {
        onPadButtonReceived: (path, button, pressed) => {
            console.warn("button", button, pressed)

            if (!pressed) {
                return
            }

            if (button === 0) {

            }
        }

    }

    PointHandler {

        id: handler
        // target: parent

        // target:

        // anchors.fill: parent

        cursorShape: Qt.WaitCursor
        acceptedDevices: PointerDevice.Stylus

        // onTapped: console.log("clicked")

        onPointChanged: console.log("point", point.pressure)
    }
}
