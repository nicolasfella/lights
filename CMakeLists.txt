cmake_minimum_required(VERSION 3.27)

set(CMAKE_AUTOMOC ON)

set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)

find_package(WaylandProtocols REQUIRED)
find_package(Wayland REQUIRED COMPONENTS Client)

find_package(Qt6 REQUIRED COMPONENTS Quick Widgets WaylandClient)
# find_package)

add_executable(lights)

target_sources(lights PRIVATE main.cpp tabletevents.cpp)

qt6_generate_wayland_protocol_client_sources(lights FILES ${WaylandProtocols_DATADIR}/unstable/tablet/tablet-unstable-v2.xml)

qt_add_qml_module(lights URI "org.kde.lights" QML_FILES Main.qml DEPENDENCIES "QtQuick")

target_link_libraries(lights PRIVATE Qt::Quick Qt::Widgets Wayland::Client Qt::WaylandClient)
