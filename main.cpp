#include <QApplication>
#include <QQmlApplicationEngine>

int main(int argc, char** argv) {
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;

    engine.loadFromModule("org.kde.lights", "Main");

    return app.exec();
}
